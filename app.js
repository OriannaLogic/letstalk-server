var restify = require('restify'),
    preflight = require('se7ensky-restify-preflight'),
    MongooseAPIFormatter = require('./formatters/mongoose_api_formatter'),
    JavascriptFormatter = require('./formatters/javascript_formatter'),
    UsersController = require('./controllers/users_controller'),
    errorHandler = require('./lib/errorHandler'),
    passport = require('passport'),
    conf = require('./conf'),
    Logger = require('bunyan'),
    auth = require('./lib/auth'),
    passportRouter = require('./lib/passportRouter'),
    securityCheck = require('./lib/securityCheck'),
    flash = require('connect-flash'),
    sessions = require("client-sessions"),
    mongoose = require('mongoose');

errorHandler.setup();

var log = new Logger({ name: 'espo-api' }),
    server = restify.createServer({
      name: 'espo-api',
      log: log,
      formatters: {
        'application/json; q=0.9': MongooseAPIFormatter,
        'application/javascript; q=0.8': JavascriptFormatter
      }
    });


mongoose.connect(conf.mongo.uri);

// Enable Cross Origin
preflight(server);
server.use(restify.CORS({

  // Authorize exprimetoi.com to make CORS
  origins: ['http://localhost:8080', 'https://' + conf.domain],
  credentials: true
}));

server.use(flash())
server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.queryParser({ mapParams: false }));

server.use(sessions({

    // cookie name dictates the key name added to the request object
    cookieName: 'session',

    // should be a large unguessable string
    secret: conf.sessionSecret,

    // how long the session will stay valid in ms
    duration: 365 * 24 * 60 * 60 * 1000 // one year
}));

// Authentication procedure
server.use(passport.initialize());
server.use(passport.session());
auth.init(passport);
passportRouter(server, passport);
server.use(securityCheck);

server.get('/users/me', UsersController.getMe);
server.put('/users/:userId', UsersController.update);
server.get('/users/random/:userId', UsersController.getRandom);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  server.listen(8002);
  log.info('API started on port 8002');
});
