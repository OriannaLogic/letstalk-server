var raven = require('raven'),
    DEBUG_MODE = false;

var ErrorHandler = {
  setup: function () {
    var _this = this;

    // Because client.patchGlobal does not allow us to get the caught error, we have to catch
    // it ourselves in order to log it.
    process.on('uncaughtException', function (err) {
      _this.handle(err);

      // Wait for sentryClient to send error and close anyway after a while.
      setTimeout(function () {
        process.exit(1);
      }, 0);
    });
  },

  handle: function (error) {
    var now = new Date();

    if (typeof error === 'string') {
      error = { message: error, stack: '' };
    }

    if (isSeriousError(error)) {
      console.log('[Error] API', '[' + now.toISOString() + ']: ' + error.message + '\n' + error.stack);
    }
  },

  log: function (error) {
    ErrorHandler.handle(error);
  }
};

function isSeriousError(error) {
  return !error.statusCode || (error.statusCode && /^5/.test(error.statusCode + ''));
}

module.exports = ErrorHandler;
