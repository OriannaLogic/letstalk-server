module.exports = function(server, passport){

  /* Handle Login POST */
  server.post('/users/login', passport.authenticate('login'), function (req, res) {
    delete req.user.password;
    res.send(req.user);
  });

  /* Handle Registration POST */
  server.post('/users/signup', passport.authenticate('signup'), function (req, res) {
    delete req.user.password;
    res.send(req.user);
  });

  /* Handle Logout */
  server.get('/users/logout', function (req, res) {
    req.logout();
    res.send(200);
    return next(false);
  });
};