var bCrypt = require('bcrypt-nodejs'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('../models/Users');

module.exports = {
  init: function (passport) {

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
      done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
      User.findById(id, function(err, user) {
        done(err, user);
      });
    });

    this.login(passport);
    this.signup(passport);
  },

  login: function(passport) {
    var loginStrategy = function(req, username, password, done) {

      // check in mongo if a user with username exists or not
      User.findOne({ 'username' :  username }, function (err, user) {

        // In case of any error, return using the done method
        if (err)
          return done(err);

        // Username does not exist, log error & redirect back
        if (!user){
          console.log('User Not Found with username '+username);
          return done(null, false,
                req.flash('message', 'User Not found.'));
        }

        // User exists but wrong password, log the error
        if (!isValidPassword(user, password)){
          console.log('Invalid Password');
          return done(null, false,
              req.flash('message', 'Invalid Password'));
        }

        // User and password both match, return user from
        // done method which will be treated like success
        req.session.userId = user._id;
        return done(null, user);
      });
    };

    passport.use('login', new LocalStrategy({
        passReqToCallback : true,
        session: true
      }, loginStrategy
    ));
  },

  signup: function(passport) {
    var signupStrategy = function(req, username, password, done) {
      var findOrCreateUser = function () {

        // find a user in Mongo with provided username
        User.findOne({ 'username': username }, function (err, user) {

          // In case of any error return
          if (err) {
            console.log('Error in SignUp: ' + err);
            return done(err);
          }

          // already exists
          if (user) {
            console.log('User already exists');
            return done(null, false,
               req.flash('message','User Already Exists'));
          } else {

            // if there is no user with that email
            // create the user
            var newUser = new User();

            // set the user's local credentials
            newUser.username = username;
            newUser.password = createHash(password);
            newUser.pseudo = username;

            // save the user
            newUser.save(function (err) {
              if (err) {
                console.log('Error in Saving user: ' + err);
                throw err;
              }

              console.log('User Registration successful');
              req.session.userId = newUser.id;
              return done(null, newUser);
            });
          }
        });
      };

      // Delay the execution of findOrCreateUser and execute
      // the method in the next tick of the event loop
      process.nextTick(findOrCreateUser);
    };

    passport.use('signup', new LocalStrategy({
        passReqToCallback : true,
        session: true
      }, signupStrategy
    ));

  }
};

var isValidPassword = function(user, password){
  return bCrypt.compareSync(password, user.password);
};

// Generates hash using bCrypt
var createHash = function(password){
 return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}