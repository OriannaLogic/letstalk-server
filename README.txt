This folder contains all the server code for my social network application.
The idea of this social network is to enable the users to speak their mind freely by assuring full data anomyzation.

It has been coded with NodeJS coupled with Mongoose and a mongoDB database.
It allows:
- randomized access to other users personal life that they share on the website
- User connection and session using the passport library
- Creation of messages
