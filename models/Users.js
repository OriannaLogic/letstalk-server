var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  pseudo: {
    type: String,
    required: true
  },
  messages: [{ title: String, content: String, date: Date }]
});

var Users = mongoose.model('Users', UserSchema);

module.exports = Users;