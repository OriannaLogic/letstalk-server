var errorHandler = require('../lib/errorHandler');

// Custom formatter that takes most of its code from the default JSON one
// But apply some tranformations to Mongoose Models to be useable as-is by
// Angular
module.exports = function(req, res, body) {

  // Copied from default formatter
  if (body instanceof Error) {

    // snoop for RestError or HttpError, but don't rely on
    // instanceof
    errorHandler.log(body);
    res.statusCode = body.statusCode || 500;

    if (body.body) {
      body = body.body;
    } else {
      body = {
        message: body.message
      };
    }
  } else if (Buffer.isBuffer(body)) {
    body = body.toString('base64');
  }

  if (body instanceof Array) {
    body = body.map(mongooseObjToJSON);
  } else {
    body = mongooseObjToJSON(body);
  }

  var data = JSON.stringify(body, undefined, 2);
  res.setHeader('Content-Length', Buffer.byteLength(data));

  return (data);
};

function mongooseObjToJSON(body) {
  if (body.toObject) {
    body = body.toObject();
  }

  if (body._id) {

    // Mongoose specific, put _id into id
    body['id'] = body['_id'];
    delete body['_id'];
  }

  if (body.__v) {
    delete body['__v'];
  }

  return body;
}
