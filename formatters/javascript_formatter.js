var errorHandler = require('../lib/errorHandler');

module.exports = function (req, res, body) {
  if (body instanceof Error) {
    errorHandler.log(body);
    res.statusCode = body.statusCode || 500;
    body = JSON.stringify(body.message);
  } else {
    body = body.toString();
  }

  res.setHeader('Content-Length', Buffer.byteLength(body));
  return (body);
};
