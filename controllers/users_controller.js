var Users = require('../models/Users'),
    restify = require('restify');

module.exports = {
  getRandom: function (req, res, next) {
    Users.find({ _id: { $ne: req.params.userId } }, { _id: 1 }, function (err, docs) {
      if (err) {
        this.error(err)
        return next(err);
      }

      c = docs.length;
      randomNumber = Math.floor(Math.random() * c)
      randomId = docs[randomNumber];

      Users.findOne({ _id: randomId }, function (err, result) {
        if (err) {
          next(err);
        } else {
          res.send(result);
          return next();
        }
      });
    });
  },

  getMe: function (req, res, next) {
    Users.findById(req.session.userId, function (err, result) {
      if (err) {
        return next(err);
      }

      res.send(result);
      return next();
    });
  },

  update: function(req, res, next) {
    var userId = req.session.userId;

    if (req.query.message) {
      var message = JSON.parse(req.query.message);
      message.date = new Date;
      Users.update(
       { _id: userId },
       { $push: { messages: message } },
       function (err) {
        if (err) {
          return next(err);
        }

        res.send(message);
        return next();
      });
    }

    if (req.query.pseudo) {
      var pseudo = req.query.pseudo;
      Users.update(
       { _id: userId },
       { pseudo: pseudo },
       function (err) {
        if (err) {
          return next(err);
        }

        res.send(200);
        return next();
      });
    }
  },

  error: function (error) {

    // TODO: Use next?
    throw new Error(error);
  }
};
